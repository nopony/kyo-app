package jsp.urlmap;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;

public class MapService {
    public void addUrl(BundleContext context) {
        try {
            HttpService http;
            ServiceReference ref = context.getServiceReference("org.osgi.service.http.HttpService");
            http = (HttpService)context.getService(ref);
            HttpContext ctx = http.createDefaultHttpContext();
            http.registerResources("/SampleApplication","WebContents", ctx);
            http.registerServlet("/SampleApplication/servlet/myservlet.do",new myappl.testServlet(), null, ctx);
            http.registerServlet("/SampleApplication/page1st.jsp",new jsp.page1st_jsp(), null, ctx);
            http.registerServlet("/SampleApplication/page2nd.jsp",new jsp.page2nd_jsp(), null, ctx);
        } catch (Exception e) {
        }
    }
    public void removeUrl(BundleContext context){
        try {
            HttpService http;
            ServiceReference ref = context.getServiceReference("org.osgi.service.http.HttpService");
            http = (HttpService)context.getService(ref);
            http.unregister("/SampleApplication");
            http.unregister("/SampleApplication/servlet/myservlet.do");
            http.unregister("/SampleApplication/page1st.jsp");
            http.unregister("/SampleApplication/page2nd.jsp");
        } catch (Exception e) {
        }
    }
}
