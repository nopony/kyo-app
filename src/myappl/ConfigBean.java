package myappl;

import java.io.Serializable;

public class ConfigBean implements Serializable {
	static String userName = "";
	private static ConfigBean instance = new ConfigBean();
	
	private ConfigBean() {
	
	};
	
	public static ConfigBean getInstance() {
		return instance;
	}
	
	public void setName(String name) {
		userName = name;
	}
	
	public String getName() {
		return userName;
	}
}
