package myappl;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sampleapplication.HyPASActivator;

import jp.co.kyoceramita.address.attribute.FtpInfo;
import jp.co.kyoceramita.attribute.Attribute;
import jp.co.kyoceramita.attribute.InvalidAttributeException;
import jp.co.kyoceramita.attribute.UnmodifiableSetException;
import jp.co.kyoceramita.box.document.attribute.FileImageQuality;
import jp.co.kyoceramita.box.document.attribute.FileName;
import jp.co.kyoceramita.box.document.attribute.StoringSize;
import jp.co.kyoceramita.job.Job;
import jp.co.kyoceramita.job.JobException;
import jp.co.kyoceramita.job.JobService;
import jp.co.kyoceramita.job.JobType;
import jp.co.kyoceramita.job.attribute.JobCreationAttributeSet;
import jp.co.kyoceramita.job.attribute.Orientation;
import jp.co.kyoceramita.job.attribute.ScanColorMode;
import jp.co.kyoceramita.job.attribute.ScanToRemovableMemoryJobCreationAttributeSet;
import jp.co.kyoceramita.job.attribute.ScanToSendJobAttributeSet;
import jp.co.kyoceramita.job.attribute.ScanToSendJobCreationAttirbuteSet;
import jp.co.kyoceramita.job.event.JobEvent;
import jp.co.kyoceramita.job.event.JobEventType;
import jp.co.kyoceramita.scan.ScanJob;
import jp.co.kyoceramita.scan.ScanWebService;
import jp.co.kyoceramita.scan.attribute.ScanResolution;
import jp.co.kyoceramita.scan.attribute.ScanSize;
import jp.co.kyoceramita.scan.event.ScanJobEvent;
import jp.co.kyoceramita.scan.event.ScanJobEventListener;
import jp.co.kyoceramita.send.attribute.FTPSendDestination;
import jp.co.kyoceramita.send.attribute.ImageQuality;
import jp.co.kyoceramita.send.attribute.PDFCompatibility;
import jp.co.kyoceramita.send.attribute.PDFEditAllowLevel;
import jp.co.kyoceramita.send.attribute.PDFFileFormat;
import jp.co.kyoceramita.send.attribute.PDFPermission;
import jp.co.kyoceramita.send.attribute.PDFPrintAllowLevel;
import jp.co.kyoceramita.send.attribute.SuffixType;

public class testServlet extends HttpServlet implements ScanJobEventListener {	
	String OCR_ADDR = "18.195.206.40";
	String OCR_USR = "kyocera";
	String OCR_PWD = "sign.govern.street.nothing.hunting.repeated";
	
	protected void startScanToFtpJob(String filename) {
		ScanWebService webService = ScanWebService.getInstance();
		webService.reserve(OCR_ADDR);
		
//		JobCreationAttributeSet scanToSendSet = JobCreationAttributeSet.newInstance(JobType.SCAN_TO_SEND);
		ScanToSendJobCreationAttirbuteSet attrSet = 
				(ScanToSendJobCreationAttirbuteSet) JobCreationAttributeSet.newInstance(JobType.SCAN_TO_SEND);
		
		try {
			attrSet.addSendDestination(new FTPSendDestination(OCR_ADDR, "", OCR_USR, OCR_PWD));
			attrSet.set(new ScanSize(jp.co.kyoceramita.job.attribute.MediaSizeName.ISO_A4, Orientation.E, false));
			attrSet.set(ScanResolution.TYPE_300x300);
			attrSet.set(ScanColorMode.FULL_COLOR);
			attrSet.set(StoringSize.A4);
			attrSet.set(new FileName(filename, SuffixType.NONE));
			PDFFileFormat pdff = new PDFFileFormat(
			PDFCompatibility.UNAVAILABLE,
			ImageQuality.PDF_IMAGE_QUALITY_1, new PDFPermission(
			PDFEditAllowLevel.ANY_EXCEPT_EXTRACTING_PAGES,
			PDFPrintAllowLevel.ALLOWED, true));
		    attrSet.set(pdff);
			
		} catch (UnmodifiableSetException e) {
			e.printStackTrace();
		} catch (InvalidAttributeException e) {
			e.printStackTrace();
		}

		
		try {
			ScanJob sendJob = (ScanJob) JobService.getInstance().create(attrSet); 
			sendJob.addListener(this);
			sendJob.start();
			System.out.println("Job registered");
		} catch (JobException e) {
			e.printStackTrace();
		}
		webService.release(OCR_ADDR);
	}
	
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ServletContext sc = getServletContext();
		
		ConfigBean config = ConfigBean.getInstance();
		String root = "/" + HyPASActivator.PLUGIN_ID;
		
		String[] params = req.getParameterValues("email");
		if(params == null) {
			sc.getRequestDispatcher(root +"/page1st.jsp").forward(req, resp);
		}
		else {
			System.out.println(params[0]);
			
			startScanToFtpJob(params[0]);
			
			sc.getRequestDispatcher(root + "/page2nd.jsp").forward(req, resp);
		}
	}
	
	public void statusChanged(ScanJobEvent event) {

		JobEventType type = (JobEventType) event.getType();
		System.out.println("job status changed to " + type);

	}



}
